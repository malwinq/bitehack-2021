import React, { Component } from 'react';
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';
import InfoIcon from '@material-ui/icons/Info';
import { withStyles } from '@material-ui/core/styles';
import { getParameters, postParameters } from './functions';

const styles = {
    panel: {
        diplay: 'inline-block',
        minWidth: '100%',
        paddingTop: '10%',
        textAlign: 'center'
    },
    button: {
        marginLeft: 30,
        marginRight: 30,
        fontFamily: 'fantasy',
        fontSize: '25px',
        backgroundColor: '#61dafb',
        color: "#041a47",
        borderRadius: '1px'
    }
}

class Panel extends Component {
    getParams = () => {
        console.log('getting...');
        const res = getParameters();
        console.log('result: ', res);
    };

    postParams = () => {
        console.log('posting...');
        postParameters();
    };

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.panel}>
                <Fab variant="extended" color="primary" aria-label="add" className={classes.button} onClick={this.postParams}>
                    <NavigationIcon className={classes.extendedIcon} />
                    Send a Task
                </Fab>
                <Fab variant="extended" color="primary" aria-label="add" className={classes.button} onClick={this.getParams}>
                    <InfoIcon className={classes.extendedIcon} />
                    Display current info
                </Fab>
            </div>
        )
    }
}

export default withStyles(styles)(Panel);