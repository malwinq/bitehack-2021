import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Panel from './Panel';

const styles = {
    header: {
        textAlign: 'center',
        minHeight: '100vh',
        top: '5%',
        fontSize: '10vmin',
        color: '#61dafb'
    },
    title: {
        fontFamily: 'fantasy',
        alignItems: 'center'
    }
}

class Header extends Component {

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.header}>
                <p className={classes.title}>EUKARIONTY</p>
                <Panel />
            </div>
        )
    }
}

export default withStyles(styles)(Header);