import cv2
import flask

app = flask.Flask(__name__)
ip='http://192.168.2.10:8080/video'
cap = cv2.VideoCapture(ip)


def get_camera_view():
    _, frame = cap.read()
    return frame


@app.route('/camera', methods=['GET'])
def send_image():
    image = get_camera_view()
    print(image)
    response = flask.make_response(image)
    response.headers.set('Content-Type', 'image/jpeg')
    print(response)
    return response


if __name__ == '__main__':
    app.run()
