# BITEhack 2021 - Eukarionty

## Robot

### Hardware
* Nvidia Jetson Xavier NX
* Arduino Mega
* Raspberry Pi Camera
...

### Software
* ROS 2

### Communication
* I2C
* UART

## Apps - web&mobile

Stack: 
* Node v12.18.3
* JavaScript
* React v17.0.1 - WEB APP
* React Native v0.62.2 - MOBILE APP
* Flask - Service for sending camera view

Libs:
* axios
* material-ui
* expo
* openCV

Web app template created by ```npx create-react-app```

## Chatbot

Microsoft Azure - Bot Framework Composer + Bot Emulator + yarn

Services used:
* Azure Bot Service
* LUIS - Language Understanding Intelligent Service
* Text Analytics API
