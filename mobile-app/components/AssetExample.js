import * as React from 'react';
import { Button, Text, View, StyleSheet, Image, TouchableOpacity, Animated } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { getParameters, postParameters } from './functions';

export default function AssetExample() {
  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={require('../assets/robot.png')} />
      <View style={styles.button4}>
        <TouchableOpacity
          style={styles.button3}
          onPress={getParams}
        >
          <Text style={styles.text}>DISPLAY CURRENT INFO</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.button5}>
        <TouchableOpacity
          style={styles.button3}
          onPress={postParams}
        >
          <Text style={styles.text}>SEND A TASK</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const getParams = () => {
    console.log('Get');
    getParameters();
}

const postParams = () => {
    console.log('Post');
    postParameters();
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 24,
    backgroundColor: '#041a47',
  },
  logo: {
    height: 128,
    width: 128,
  },
  button: {
    paddingTop: 30,
    color: '#041a47',
  },
  button2: {
    paddingTop: 30,
    color: '#041a47'
  },
  button3: {
    textColor: "#041a47",
    backgroundColor: "#61dafb",
    alignItems: "center",
    fontFamily: 'fantasy',
    fontWeight: 'bold',
    padding: 10,
  },
  button4: {
    paddingTop: 120,
  },
  button5: {
    paddingTop: 30
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'fantasy'
  }
});
