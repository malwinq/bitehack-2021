import axios from 'axios';

const url = "http://192.168.2.6:8080"

export const getParameters = () => {
    let result;
    axios.get(url + "/app")
        .then(response => {
            result = response;
        })
        .catch(error => {
            console.error('ERROR ON GET: ', error);
        });
    return result;
};

export const postParameters = (params) => {
    axios.post(url + "/app", params)
        .then(response => {
            console.log('POST PARAMS, response: ', response);
        })
        .catch(error => {
            console.log('ERROR ON POST: ', error);
        });
}