import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>
        EUKARIONTY
      </Text>
      <Card>
        <AssetExample />
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#041a47',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 45,
    fontWeight: 'bold',
    fontFamily: 'fantasy',
    textAlign: 'center',
    color: '#61dafb'
  },
});
